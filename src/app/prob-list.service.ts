import { Injectable } from '@angular/core';
import { PROBLEMLIST } from './ProblemList';

@Injectable({
  providedIn: 'root'
})
export class ProbListService {
  pList = PROBLEMLIST;

  constructor() { }

  public getProblem(p: string) {
    return this.pList[p];
  }
}
