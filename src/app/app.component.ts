import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'euler';
  currentRoute = '';
  breadcrumbs = [];

  constructor() { }

  light() {
    document.documentElement.classList.add('color-theme-in-transition');
    document.documentElement.setAttribute('data-theme', null);
    window.setTimeout(function() {
      document.documentElement.classList.remove('color-theme-in-transition')
    }, 1000);
  }
  dark() {
    document.documentElement.classList.add('color-theme-in-transition');
    document.documentElement.setAttribute('data-theme', 'dark');
    window.setTimeout(function() {
      document.documentElement.classList.remove('color-theme-in-transition')
    }, 1000);
  }
  experimental() {
    document.documentElement.classList.add('color-theme-in-transition');
    document.documentElement.setAttribute('data-theme', 'hawaiian');
    window.setTimeout(function() {
      document.documentElement.classList.remove('color-theme-in-transition')
    }, 1000);
  }
}
