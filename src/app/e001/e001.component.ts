import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-e001',
  templateUrl: './e001.component.html',
  styleUrls: ['./e001.component.scss']
})
export class E001Component implements OnInit {
  problem = 'e001';
  url = 'https://projecteuler.net/problem=1';
  problemTitle = 'Multiples of 3 and 5';
  problemDesc = 'Find the sum of all the multiples of 3 or 5 below <target>';
  resStr = 'The sum of all multiples of 3 or 5 below';
  target = null;
  result = null;

  addForm: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { 
    this.addForm = fb.group({
      Target: ['', Validators.required],
    });
  }

  ngOnInit() {
    document.documentElement.setAttribute('data-theme', 'dark');
    // document.documentElement.setAttribute('data-theme', null);
  }

  add(form: FormGroup): void {
    // console.log(form);
    if (form.value.Target) {
      this.target = form.value.Target;
      this.result = this.multiples_3_5(this.target);
    } else {
      console.error("No target given");
    }
  }

  multiples_3_5(target: number) {
    let sum = 0;

    for (let i = 1; i < target; i++) {
      if (i % 3 === 0 || i % 5 ===0 ) 
        sum += i;
    }
    return sum;
  }

}
