export class Problem {
    problem:      string;
    url:          string;
    problemTitle: string;
    problemDesc:  string;
    resStr:       string;
}