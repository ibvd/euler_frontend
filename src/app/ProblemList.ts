import { multiples_3_5, even_fib_sums, largest_palindromic_product, smallestMultiple, sumSquareDifference, largestPrimeFactor, trialDivision } from './lib/solutions';

export const PROBLEMLIST = {
    '1': {
        'problem': 'e001',
        'url': 'https://projecteuler.net/problem=1',
        'problemTitle': 'Multiples of 3 and 5',
        'problemDesc': 'Find the sum of all the multiples of 3 or 5 below <target>',
        'resStr': 'The sum of all multiples of 3 or 5 below',
        'solution': multiples_3_5
    },
    '2': {
        'problem': 'e002',
        'url': 'https://projecteuler.net/problem=2',
        'problemTitle': 'Even Fibonacci numbers',
        'problemDesc': 'Find the sum of all the even fibonacci numbers less than <target>',
        'resStr': 'The sum of even fibonacci numbers below',
        'solution': even_fib_sums
    },
    '3': {
        'problem': 'e003',
        'url': 'https://projecteuler.net/problem=3',
        'problemTitle': 'Largest prime factor',
        'problemDesc': 'What is the largest prime factor of <target>',
        'resStr': 'The largest prime factor of',
        'solution': trialDivision
    },
    '4': {
        'problem': 'e004',
        'url': 'https://projecteuler.net/problem=4',
        'problemTitle': 'Largest Palindrome Product',
        'problemDesc': 'Find the largest palindrom made from the product of two n-digit numbers',
        'resStr': 'The largest palindromic product of',
        'solution': largest_palindromic_product
    },
    '5': {
        'problem': 'e005',
        'url': 'https://projecteuler.net/problem=5',
        'problemTitle': 'Smallest Multiple',
        'problemDesc': 'What is the smallest positive number that is evenly divisible by all the numbers from 1 to 20?',
        'resStr': 'The smallest multiple evenly divisiable by all numbers from 1 to',
        'solution': smallestMultiple
    },
    '6': {
        'problem': 'e006',
        'url': 'https://projecteuler.net/problem=6',
        'problemTitle': 'Sum Square Difference',
        'problemDesc': 'Find the difference between the sum of the squaresw of the first one hundred natural numbers and the square of the sum.',
        'resStr': 'The difference between the sum of Squares and square of the sum of:',
        'solution': sumSquareDifference
    },
    '7': {
        'problem': 'e007',
        'url': 'https://projecteuler.net/problem=7',
        'problemTitle': '10001st prime',
        'problemDesc': 'What is the 10001st prime number?',
        'resStr': 'The nth prime number for:',
        'solution': sumSquareDifference
    },
    '8': {
        'problem': 'e008',
        'url': 'https://projecteuler.net/problem=8',
        'problemTitle': 'Largest product in a series',
        'problemDesc': 'Find the product of the 13 adjacent digits in the bignum that have the greatest product',
        'resStr': 'The product of the digits is',
        'solution': sumSquareDifference
    },
    '9': {
        'problem': 'e009',
        'url': 'https://projecteuler.net/problem=9',
        'problemTitle': 'Special Pythagorean Triplet',
        'problemDesc': 'Find the product of a*b*c for the Pythagorean Triplet where a+b+c = 1000',
        'resStr': 'The product of a*b*c',
        'solution': sumSquareDifference
    },
    '10': {
        'problem': 'e010',
        'url': 'https://projecteuler.net/problem=10',
        'problemTitle': 'Summation of primes',
        'problemDesc': 'Find the sum of all the primes below <target>',
        'resStr': 'The sum of all primes below',
        'solution': sumSquareDifference
    },
}
