import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Result } from './result';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SolverService {
  constructor(
    private http: HttpClient,
  ) { }

  getSolution(problem: string, n: number): Observable<Result> {
    let url = environment.apiUrl + `euler?problem=${problem}&n=${n}`;
    return this.http.get<Result>(url);
  }
}
