import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, PRIMARY_OUTLET } from '@angular/router';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  currentRoute = '';
  breadcrumbs = [];

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    /*
    this.router.events
      .pipe(filter( event => event instanceof NavigationEnd ))
      .subscribe( (url:any) => { 
        if (typeof url.url !== 'undefined') {
          console.log(this.actRoute.snapshot.paramMap.get('id'));
          this.currentRoute = 'problem ' + url['id'];
          console.log(url);
          console.log(this.actRoute.snapshot.paramMap);
        }
      });
      */

    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .pipe(map(() => this.actRoute))
      .pipe(map((route) => {
        while (route.firstChild) { route = route.firstChild; }
        return route;
      }))
      .pipe(filter(route => route.outlet === PRIMARY_OUTLET))
      .subscribe( route => {
        let snapshot = this.router.routerState.snapshot;
        this.breadcrumbs = [];
        let url = snapshot.url;
        let routeData = route.snapshot.data;

        let label = routeData['breadcrumb'];
        let params = snapshot.root.queryParams;

        this.breadcrumbs.push({
          url: url.substring(1),
          label: label,
          params: params
        });
        this.currentRoute = url;
      });
  }
}
