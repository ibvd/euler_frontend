import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ProbListService } from '../prob-list.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SolverService } from '../services/solver.service';
import { Observable } from 'rxjs';
import { Result } from '../services/result';


@Component({
  selector: 'app-problem',
  templateUrl: './problem.component.html',
  styleUrls: ['./problem.component.scss']
})
export class ProblemComponent implements OnInit {
  addForm: FormGroup;
  target = null;
  result = null;
  id = '0';
  p;

  constructor(
    private fb: FormBuilder,
    private pList: ProbListService,
    private route: ActivatedRoute,
    private router: Router,
    private solver: SolverService,
  ) { 
    this.addForm = fb.group({
      Target: ['', Validators.required],
    });
  }

  ngOnInit() {
    document.documentElement.setAttribute('data-theme', 'dark');
    // Handle first page load
    this.id = this.route.snapshot.paramMap.get('id');
    this.p = this.pList.getProblem(this.id);

    // Detect routing changes and update accordingly
    this.router.events
      .pipe(filter( event => event instanceof NavigationEnd ))
      .subscribe( (url:any) => { 
        if (typeof url.url !== 'undefined') {
          this.id = this.route.snapshot.paramMap.get('id');
          this.p = this.pList.getProblem(this.id);
        }
        // console.log(url);
      });
  }

  solve(form: FormGroup): void {
    // console.log(form);
    if (form.value.Target) {
      this.target = form.value.Target;
      this.solver.getSolution(this.p.problem, this.target)
        .subscribe(res => {
          this.result = res.result;
          console.log('result:', this.result);
      });
      // this.result = this.p.solution(this.target);
    } else {
      console.error("No target given");
    }
  }
}
