import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProblemComponent } from './problem/problem.component'

const routes: Routes = [
  {path: '', redirectTo: 'problem/1', pathMatch: 'full' },
  {path: 'problem/:id', component: ProblemComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
