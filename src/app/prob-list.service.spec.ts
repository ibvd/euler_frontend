import { TestBed } from '@angular/core/testing';

import { ProbListService } from './prob-list.service';

describe('ProbListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProbListService = TestBed.get(ProbListService);
    expect(service).toBeTruthy();
  });
});
