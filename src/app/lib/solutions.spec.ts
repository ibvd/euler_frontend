import { TestBed } from '@angular/core/testing';
import * as s from './solutions';


describe('largestPrimeFactor', () => {
  it('largestPrimeFactore(10) should be 5', () => {
    let res = s.largestPrimeFactor(10);
    expect(res).toBe(5);
  });
  it('largestPrimeFactore(13195) should be 29', () => {
    let res = s.largestPrimeFactor(13195);
    expect(res).toBe(29);
  });
});

describe('trialDivision', () => {
  it('trialDivision(10) should be 5', () => {
    let res = s.trialDivision(10);
    console.log(10, res);
    expect(res).toEqual(5);
  });
  it('trialDivision(20) should be 5', () => {
    let res = s.trialDivision(20);
    console.log(20, res);
    expect(res).toEqual(5);
  });
  it('trialDivision(13195) should be 29', () => {
    let res = s.trialDivision(13195);
    console.log(13195, res);
    expect(res).toEqual(29);
  });
});

describe('primeList', () => {
  it('primeList(10) should be [2, 3, 5, 7]', () => {
    let res = s.primeList(10);
    expect(res).toEqual([2,3,5,7]);
  });
  it('primeList(20) should be [2, 3, 5, 7, 11, 13, 17, 19]', () => {
    let res = s.primeList(20);
    expect(res).toEqual([2,3,5,7,11,13,17,19]);
  });
});

describe('isPrime', () => {
  it('isPrime(7) should be true', () => {
    let res = s.isPrime(7);
    expect(res).toBeTruthy();
  });
  it('isPrime(31) should be true', () => {
    let res = s.isPrime(31);
    expect(res).toBeTruthy();
  });
  it('isPrime(18) should be false', () => {
    let res = s.isPrime(18);
    expect(res).toBeFalsy();
  });
});


describe('sumSquareDifference', () => {
  it('sumSquareDifference(10) should equal 2640', () => {
    let res = s.sumSquareDifference(10);
    expect(res).toBe(2640);
  });
});
describe('squareSums', () => {
  it('squareSums(10) should equal 3025', () => {
    let res = s.squareSums(10);
    expect(res).toBe(3025);
  });
});
describe('sumSquares', () => {
  it('sumSquares(10) should equal 385', () => {
    let res = s.sumSquares(10);
    expect(res).toBe(385);
  });
});

describe('Smallest Multiple', () => {
  it('smallestMultiple(10) should equal 2520', () => {
    let res = s.smallestMultiple(10);
    expect(res).toBe(2520);
  });
  it('smallestMultiple(20) should equal 232792560', () => {
    let res = s.smallestMultiple(20);
    expect(res).toBe(232792560);
  });
});

describe('largest_palindromic_product', () => {
  it('largest_palindromic_product(2) should be 9009', () => {
    let res = s.largest_palindromic_product(2);
    expect(res).toBe(9009);
  });
  it('largest_palindromic_product(3) should be 906609', () => {
    let res = s.largest_palindromic_product(3);
    expect(res).toBe(906609);
  });
});

describe('isPalindromic', () => {
  it('9009 should be true', () => {
    let res = s.is_palindromic(9009);
    expect(res).toBeTruthy();
  });
  it('90709 should be true', () => {
    let res = s.is_palindromic(90709);
    expect(res).toBeTruthy();
  });
  it('90719 should be false', () => {
    let res = s.is_palindromic(90719);
    expect(res).toBeFalsy();
  });
  it('9019 should be false', () => {
    let res = s.is_palindromic(9019);
    expect(res).toBeFalsy();
  });
});

describe('Even Fibonacci Sums', () => {
  let res1 = s.even_fib_sums(10);
  it('10 should equal 10', () => {
    expect(res1).toEqual(10);
  });

  let res2 = s.even_fib_sums(50);
  it('50 should equal 44', () => {
    expect(res2).toEqual(44);
  });
});

describe('Multiples_3_5', () => {
  let res1 = s.multiples_3_5(10);
  it('10 should equal 23', () => {
    expect(res1).toEqual(23);
  });

  let res2 = s.multiples_3_5(1000);
  it('1000 should equal 233168', () => {
    expect(res2).toEqual(233168);
  });
});

describe('Zero', () => {
  let res = s.zero(15);

  it('should equal 42', () => {
    expect(res).toEqual(42);
  });
});