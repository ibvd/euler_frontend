import { ModuleWithComponentFactories } from "@angular/core";

/* ********************************* *
 * Problem 06: Sum square Difference *
 * **********************************/
export function squareSums(target: number): number {
  let sum = 0;
  for (let i = 1; i <= target; i++) {
    sum += i;
  }
  return Math.pow(sum, 2);
}

export function sumSquares(target: number): number {
  let sum = 0;
  for (let i = 1; i <= target; i++) {
    sum += Math.pow(i, 2);
  }
  return sum;
}

export function sumSquareDifference(target: number) {
  let res1 = sumSquares(target);
  let res2 = squareSums(target);
  return res2 - res1;
}

/* ***************************** *
 * Problem 05: Smallest Multiple *
 * ******************************/
export function smallestMultiple(target: number) {
  for (let i = target; ; i++) {
    for (let x = 1; x <= target; x++ ) {
      if (i % x !== 0) {
        break;
      } else {
        if (x === target) { return i; }
      }
    }
  }
}

/* ************************************* *
 * Problem 04: Largest Palindrom Product *
 * **************************************/
export function is_palindromic(num: number) {
  let result = false;
  let first = ''; let second = '';

  let s = ''+num; // convert to string
  if (s.length % 2 === 0) { // Handle even length strings
    first = s.substr(0, s.length / 2);
    second = s.substr(s.length/2).split('').reverse().join('');
  } else { // odd length string
    first = s.substr(0, s.length / 2);
    second = s.substr(s.length/2 + 1).split('').reverse().join('');
  }
  if (first === second) { 
    return true 
  } 
  return false 
}

export function largest_palindromic_product(n: number) {
  let max=0;
  for (let x = Math.pow(10,n) - 1; x >= 1; x-- ) {
    for (let y = Math.pow(10,n) -1; y >= x; y-- ) {
      if (x*y <= max) { break }
      if (is_palindromic(x*y)) { max = x*y }
    }
  }
  return max;
}

/* ******************************** *
 * Problem 03: Largest Prime Factor *
 * *********************************/
export function isPrime(n: number): boolean {
  for( let x = 2; x < n; x++ ) {
    if( n % x === 0 ) { return false; }
  }
  return true;
}

export function primeList(n: number): Array<number> {
  let primes = [2,];
  for( let p = 3; p <= n; p += 2) {
    if( isPrime(p) === true) {
      primes.push(p);
    }
  }
  return primes;
}

export function trialDivision(n: number): number {
  let primeFac: Array<number> = [0,];
  let num = n;

  if( num % 2 === 0 ) {
    primeFac.push(2);
    num = Math.floor(num/2);
  }
  let factor = 3;
  while( factor <= num ) {
    if( num % factor === 0 ) {
      primeFac.push(factor);
      num = Math.floor(num/factor);
    } else {
      factor += 2;
    }
  }

  if( num !== 1 ) { primeFac.push(num); }

  for( let v of primeFac ) {
    if( v % 2 === 0 && v !== 2 ) {
      primeFac.splice( primeFac.indexOf(v), 1);
    }
  }

  return primeFac.reduce( (p, v) => { return (p>v ? p : v); });
}

export function largestPrimeFactor(n: number): number {
  let cap = Math.floor(n / 2);
  let primes = primeList(cap).reverse();
  let factors: Array<number>;

  // iterate through PrimeList from the largest prime to smallest
  for (let p of primes) {
    if( n % p === 0 ) {
      return p;
    }
  }
  return 0;
}


/* ********************************** *
 * Problem 02: Even Fibonacci Numbers *
 * ************************************/
export function even_fib_sums(target: number) {
  let sum = 0;
  let n0 = 0; let n1 = 1; let n2 = 1;
  while (n2 <= target) {
    if (n2 % 2 === 0) { sum += n2; }
    n0 = n1;
    n1 = n2;
    n2 = n0 + n1;
  }
  return sum;
}

/* ****************************** *
 * Problem 01: Multiples of 3 & 5 *
 * *******************************/
export function multiples_3_5(target: number) {
  let sum = 0;

  for (let i = 1; i < target; i++) {
    if (i % 3 === 0 || i % 5 ===0 ) 
      sum += i;
  }
  return sum;
} 

/* ***************
 * Test Function *
 * **************/
export function zero(target: number) {
  return 42;
}