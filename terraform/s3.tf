resource "aws_s3_bucket" "b" {
  bucket = "${var.app_name}.${var.dns_zone}"

  acl = "public-read"
  policy = templatefile("s3-cloudflare-policy.json", { bucket = "${var.app_name}.${var.dns_zone}" })

  website {
    index_document = "index.html"
    // error_document = "404.html"
    routing_rules = <<EOF
[{
    "Condition": {
        "HttpErrorCodeReturnedEquals": "404"
    },
    "Redirect": {
        "ReplaceKeyWith": ""
    }
}]
EOF
  }

  tags = {
    Name        = "Project Euler Solver"
    Environment = "Prd"
  }
}
