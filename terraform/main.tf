terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  cloud {
    organization = "ibvd"
    workspaces {
      name = "euler_frontend"
    }
  }
}

## Configure the CloudFlare provider
provider "cloudflare" {
}

## Configure the AWS provider
provider "aws" {
  region = var.aws_region
}
