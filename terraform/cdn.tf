data "cloudflare_zone" "zone" {
  name                = var.dns_zone
}

resource "cloudflare_record" "cname" {
  name                = var.app_name
  type                = "CNAME"
  zone_id             = data.cloudflare_zone.zone.id
  value               = "${var.app_name}.${var.dns_zone}.s3-website-${var.aws_region}.amazonaws.com"
  proxied             = true
  allow_overwrite     = true
}

resource "cloudflare_page_rule" "pgr" {
  zone_id             = data.cloudflare_zone.zone.id
  target              = "${var.app_name}.${var.dns_zone}"
  priority            = 1

  actions {
    always_use_https  = true
  }
}
