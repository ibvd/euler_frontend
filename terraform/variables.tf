variable "app_name" {
  type        = string
  description = "name of app: e.g. my_app.  This will end up being dns CNAME record as in my_app.example.com"
}

variable "dns_zone" {
  type        = string
  description = "DNS Zone for the public interface of the vm.  e.g. example.com"
}

variable "aws_region" {
  type        = string
  description = "AWS Region. e.g. us-east-1"
}
