# Project Euler Solver

Terraform codebase to setup AWS & Cloudflare resources for hosting the Project Euler Angular application.

## Architecture

Public Angular apps are pretty easy to host and so the infrastrucutre requirements are small.  However there are still a few constraints we want to put in place:

- All traffic must flow through CloudFlare and not hit the bucket directly.
  - e.g. you must go to app.example.com  not app.example.com.s3-website-us-east-1.amazonaws.com
  - This will be enforced by using a bucket policy to restrict access to known CloudFlare Proxy IP addresses
  - We will also setup Cloudflare DNS to proxy and cache hits to the page.
- All traffic must use https and use a valid cert
  - CloudFlare page rules will handle the automatic redirect
- Deep links will not be supported.  Since we are using a SPA, all access will go through the app.
  - S3 polcies will enforce that only the SPA can load and prevent a 404 on a reload of the browser or invalid deep link.


### What is created
This Terraform will create:

- Cloudflare DNS entries
- Cloudflare PageRules
- The AWS S3 bucket 
- S3 bucket policies

### What is not created
This terraform assumes the following already exist:

- Cloudflare DNS Zones
- Terraform Cloud workspace with permissions to manipulate AWS & Cloudflare
- GitLab repo & permissions

