#docker run --name euler_frontend -p 3000:3000 --rm -v `pwd`:/src -v nodemodules:/src/node_modules -w /src node:14.19  npm start

FROM node:14.19

WORKDIR /src
COPY package.json package-lock.json /src/
RUN npm install
#RUN npm install -g @angular/cli@13.2
COPY . /src/
EXPOSE 4200

CMD ["npm", "start"]
