# Euler Frontend

This is toy project for solving a few Project Euler problems using a serverless architecture.  
This repo provides the Angular frontend (originally 8 but now on 13) as well as the terraform to provision the hosting environment, and the gitlab CI/CD to deploy into that environment.

The backend is a Lambda ([AWS Serverless Application Model](https://aws.amazon.com/serverless/sam/)) REST API which provides the answers to the problems.

The deployed version of the application is viewable @ [Solver Website](https://ibvdeuler.vandevenders.com/).
The backend code is hosted @ [Euler Backend](https://gitlab.com/ibvd/euler_backend/).



## Stack

| Layer    | Solution         | Notes            |
|----------|------------------|------------------|
| Frontend | Angular 13       |                  |
| Website  | AWS S3           |                  |
| CDN      | Cloudflare       |                  |
| Backend  | AWS Serverless   | Python on Lambda |
| Certs    | Cloudflare / AWS |                  |
| VCS      | Gitlab           |                  |
| CI/CD    | Gitlab           |                  |
| IaC      | Terraform Cloud  |                  |
