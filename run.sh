#!/bin/bash

docker=/usr/bin/docker
project="euler_frontend"
port=4200

$docker run --name $project -p $port:$port --rm -v `pwd`:/src -v nodemodules:/src/node_modules -w /src ibvd/$project:latest npm start
